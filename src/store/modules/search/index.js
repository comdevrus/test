import API from '@api';
import FilterPrice from '@domain/FilterPrice';
import FilterFootage from '@domain/FilterFootage';

const getDefaultState = () => ({
    searched: [],
    filter: {
        price: new FilterPrice({
            start: null,
            end: null,
            currency: null
        }),
        footage: new FilterFootage({
            start: null,
            end: null
        })
    }
});

const state = getDefaultState();

export default {
    namespaced: true,
    state,
    actions: {
        async getSearched({ commit }, params) {
            const searched = await API.search.getByName(params);
            commit('setSearched', searched);
        },
        async getSearchedByFilter({ commit }, params) {
            const searched = await API.search.getByFilter(params);
            commit('setSearched', searched);
        },
        async setFilterPrice({ commit }, params) {
            const filterPrice = new FilterPrice({
                start: params.start,
                end: params.end,
                currency: params.currency
            });
            commit('setFilterPrice', filterPrice);
        },
        async setFilterFootage({ commit }, params) {
            const filterFootage = new FilterFootage({
                start: params.start,
                end: params.end
            });
            commit('setFilterFootage', filterFootage);
        },
        resetState({ commit }) {
            commit('resetState');
        }
    },
    mutations: {
        setSearched(state, payload) {
            state.searched = payload;
        },
        setFilterPrice(state, payload) {
            state.filter.price = payload;
        },
        setFilterFootage(state, payload) {
            state.filter.footage = payload;
        },
        resetState(state) {
            Object.assign(state, getDefaultState());
        }
    },
    getters: {
    }
};
