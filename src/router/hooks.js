export default (router) => {
    router.beforeResolve((to, from, next) => {
        next();
    });

    return router;
};
