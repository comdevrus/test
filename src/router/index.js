import Vue from 'vue';
import Router from 'vue-router';
import routes from './routes';
import hooks from './hooks';

Vue.use(Router);

export default hooks(
    new Router({ routes, mode: 'history' })
);
