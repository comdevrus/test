export default {
    path: '/',
    name: 'main',
    component: () => import('@views/search/Search'),
    meta: {
        title: 'Поиск',
        guest: true
    }
};
