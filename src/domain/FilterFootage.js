/**
 * Класс для фильтра Footage
 */
export default class FilterFootage {
    /**
     * Конструктор
     * @param {Object} origin Данные
     */
    constructor(origin) {
        this.start = origin.start;
        this.end = origin.end;
    }
}
