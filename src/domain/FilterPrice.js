/**
 * Класс для фильтра Price
 */
export default class FilterPrice {
    /**
     * Конструктор
     * @param {Object} origin Данные
     */
    constructor(origin) {
        this.start = origin.start;
        this.end = origin.end;
        this.currency = origin.currency || 1;
    }
}
