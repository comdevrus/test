import dat from '@api/search/data.json';

const Search = {
    getByName(params) {
        return dat.filter(e => e.name.toLowerCase().indexOf((params.params || '').toLowerCase()) !== -1);
    },

    getByFilter(params) {
        return dat.filter((e) => {
            if (e.region.id !== params.region) {
                return false;
            }

            if (e.action.id !== params.action) {
                return false;
            }

            if (e.object.id !== params.object) {
                return false;
            }

            if (params.footage) {
                if (params.footage.start > 0 && params.footage.start > e.footage) {
                    return false;
                }

                if (params.footage.end > 0 && params.footage.end < e.footage) {
                    return false;
                }
            }

            if (params.price) {
                if (params.price.start > 0 && params.price.start > e.price) {
                    return false;
                }

                if (params.price.end > 0 && params.price.end < e.price) {
                    return false;
                }

                if (params.price.currency !== e.priceType) {
                    return false;
                }
            }

            return true;
        });
    }
};

export default Search;
