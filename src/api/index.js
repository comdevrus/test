import search from './search';

const API = {
    search
};

export default API;
